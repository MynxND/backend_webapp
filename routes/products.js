const express = require('express')
const router = express.Router()
const Product = require('../models/Product')

const getProducts = async (req, res, next) => {
  try {
    const products = await Product.find({}).exec()
    res.json(products)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getProduct = async function (req, res, next) {
  const id = req.params.id
  console.log(id)
  try {
    const product = await Product.findById(id).exec()
    if (product === null) {
      return res.status(404).json({
        message: 'Product not found.'
      })
    }
    res.json(product)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addProducts = async (req, res, next) => {
  const newProduct = new Product({
    name: req.body.name,
    price: parseFloat(req.body.price)
  })
  try {
    await newProduct.save()
    res.status(201).json(newProduct)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateProduct = async (req, res, next) => {
  const productId = req.params.id
  try {
    const product = await Product.findById(productId)
    product.name = req.body.name
    product.price = parseFloat(req.body.price)
    await product.save()
    return res.status(200).json(product)
  } catch (err) {
    return res.status(404).send({ messag: err.messag })
  }
}

const deleteProduct = async (req, res, next) => {
  const productId = req.params.id
  try {
    await Product.findByIdAndDelete(productId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }

  // const index = products.findIndex(function (item) {
  //   return item.id === productId
  // })
  // if (index >= 0) {
  //   products.splice(index, 1)
  //   res.status(204).send()
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No product id ' + req.params.id
  //   })
  // }
}

router.get('/', getProducts) // GET Products
router.get('/:id', getProduct) // GET One Product
router.post('/', addProducts) // Add New Product
router.put('/:id', updateProduct) // Add New Product
router.delete('/:id', deleteProduct) // Delete Product

module.exports = router
