const mongoose = require('mongoose')
const Event = require('../models/event')
mongoose.connect('mongodb://localhost:27017/example')
async function clear () {
  await Event.deleteMany({})
}

async function main () {
  await clear()
  await Event.insertMany([
    {
      title: 'One', content: 'Content 1', startDate: new Date('2022-03-03 08:00'), endtDate: new Date('2022-03-03 16:00'), class: 'a'
    },
    {
      title: 'Two', content: 'Content 2', startDate: new Date('2022-03-30 08:00'), endtDate: new Date('2022-03-30 16:00'), class: 'b'
    },
    {
      title: 'Three', content: 'Content 3', startDate: new Date('2022-03-20 08:00'), endtDate: new Date('2022-03-20 16:00'), class: 'c'
    },
    {
      title: 'Four', content: 'Content 4', startDate: new Date('2022-03-21 08:00'), endtDate: new Date('2022-03-21 12:00'), class: 'a'
    },
    {
      title: 'Five', content: 'Content 5', startDate: new Date('2022-03-21 13:00'), endtDate: new Date('2022-03-21 16:00'), class: 'b'
    }
  ])
  Event.find({ })
}

main().then(function () {
  console.log('Finish')
})
